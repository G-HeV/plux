#!/bin/bash

FILE=$1/*;

for file in $FILE
do
	lines=$(cat $file | wc -l);
	chars=$(cat $file | wc -m);
	owner=$(ls -ld $file | awk '{print $3}');
	echo "$file $owner $lines $chars  " ;
	echo "$file $owner $lines $chars  " >> $2;
done

exit 0;