#include <pthread.h>
#include <stdio.h>

struct char_print_parms {
	char character;
	int count;
};
void* char_print (void* parameters) {
	struct char_print_parms* p = (struct char_print_parms*)parameters;
	int i;
	for (i = 0; i < p->count; ++i)
		fputc (p->character, stderr);
	printf("\n");
	return NULL;
}
int main () {
	pthread_t thread1_id;
	char znak1;
	struct char_print_parms thread1_args;
	printf("podaj znak\n");
	scanf("%c", &znak1);
	thread1_args.character = znak1;
	thread1_args.count = 100;
	pthread_create(&thread1_id, NULL, &char_print,&thread1_args);
	pthread_join(thread1_id, NULL);
	return 0;
}