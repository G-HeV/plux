#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

struct char_print_parms {
	char character;
	int count;
};
void* char_print (void* parameters) {
	struct char_print_parms* p = (struct char_print_parms*)parameters;
	int i;
	for (i = 0; i < p->count; ++i)
		fputc (p->character, stderr);
	printf("\n");
	return NULL;
}
int main (void) {
	pthread_t thread1_id;
	pthread_t thread2_id;
	char znak1;
	char znak2;
	struct char_print_parms thread1_args;
	struct char_print_parms thread2_args;

	printf("podaj znak\n");
	scanf(" %c", &znak1);
	thread1_args.character = znak1;
	thread1_args.count = 10;
	printf("podaj drugi znak\n");
	scanf(" %c", &znak2);
	thread2_args.character = znak2;
	thread2_args.count= 20;
	pthread_create(&thread1_id,NULL, &char_print,&thread1_args);
	pthread_create(&thread2_id,NULL, &char_print,&thread2_args);
	return 0;
}
/*funkcja main moze zakonczyc dzialanie przed zakonczeniem dzialania watkow, drugi
watek moze sie nie wykonac.*/