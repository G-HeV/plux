#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
sig_atomic_t child_exit_status;
void clean_up_child_process (int signal_number) {
int status;
wait (&status);
child_exit_status = status;
}



int main()
{
    pid_t child_pid;
    int status;
    int test = 5;
printf("Proces o id:%d\n",(int) getpid());
	system("ps");
    /* create process fork() */
    child_pid = fork();

    if (child_pid >= 0) /* if success */
    {
        if (child_pid == 0) /* fork() returns 0 for the child process */
        {
            printf("child process\n");

        
            int test2=200;
            

            printf("child PID =  %d, parent pid = %d\n", getpid(), getppid());
            printf("\n child's test = %d, child's test2 = %d\n",test,test2);
		system("ls -l /");
	system("ps -aux | grep zadanie.c");
           sleep(10);
    system("ls -l /");
	system("ps -aux | grep zadanie.c");
     	return 0;

         }
         else /* parent process */
         {
	int test2=300;
             printf("parent process!\n");
             printf("parent PID =  %d, child pid = %d\n", getpid(), child_pid);
             wait(&status); /* wait for child to exit, and store child's exit status */
             printf("Child exit code: %d\n", WEXITSTATUS(status));
		
             //The change in local and global variable in child process should not reflect here in parent process.
             printf("\n Parent'z test = %d, parent's  test2 = %d\n",test,test2);
		 

	struct sigaction sigchld_action;
	memset (&sigchld_action, 0, sizeof (sigchld_action));
	sigchld_action.sa_handler = &clean_up_child_process;
	sigaction (SIGCHLD, &sigchld_action, NULL);
             exit(0);  
         }
    }
    else /* error */
    {
        perror("fork");
        exit(0);
    }
}
